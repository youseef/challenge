package day7;

import java.util.*;

public class Main {
    public static void main(String[] arg) {
        reverseString(new char[]{'h','e','l','l','o'});
    }

    public static String mergeAlternately(String word1, String word2) {
        StringBuilder result = new StringBuilder();
        int i = 0;
        while (i < word1.length() || i < word2.length()) {
            if (i < word1.length()) {
                result.append(word1.charAt(i));
            }
            if (i < word2.length()) {
                result.append(word2.charAt(i));
            }
            i++;
        }
        return result.toString();
    }

    public static int countKeyChanges(String s) {
        int count = 0;
        for (int i = 1; i < s.length(); i++) {
            if (s.toLowerCase().charAt(i) != s.toLowerCase().charAt(i - 1)) {
                count++;
            }
        }
        return count;
    }

    public static boolean halvesAreAlike(String s) {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilder1 = new StringBuilder();
        for (int i = 0; i < s.length() / 2; i++) {
            stringBuilder.append(s.charAt(i));
        }
        for (int i = s.length() / 2; i < s.length(); i++) {
            stringBuilder1.append(s.charAt(i));
        }
        List<Character> vowel1 = vovelList(stringBuilder.toString());
        List<Character> vowel2 = vovelList(stringBuilder1.toString());

        return vowel1.size() == vowel2.size();
    }

    public static List<Character> vovelList(String s) {
        List<Character> vovels = new ArrayList<>();
        for (char ch : s.toCharArray()) {
            if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' || ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U') {
                vovels.add(ch);
            }
        }
        return vovels;
    }

    public static List<String> fizzBuzz(int n) {
        List<String> numbers = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                numbers.add("FizzBuzz");
            } else if (i % 3 == 0) {
                numbers.add("Fizz");
            } else if (i % 5 == 0) {
                numbers.add("Buzz");
            } else {
                numbers.add(Integer.toString(i));
            }
        }
        return numbers;
    }

    public static String removeTrailingZeros(String num) {
        int count = 0;
        for (int i = num.length() - 1; i >= 0; i--) {
            char ch = num.charAt(i);
            if (ch != '0') {
                break;
            } else
                count++;
        }
        return num.substring(0, num.length() - count);
    }

    public static boolean squareIsWhite(String coordinates) {
        char letter = coordinates.charAt(0);
        int number = Character.getNumericValue(coordinates.charAt(1));

        return (letter - 'a' + number) % 2 == 0;
    }

    public static String generateTheString(int n) {
        StringBuilder stringBuilder = new StringBuilder(n);
        char x = 'x';
        char y = 'y';
        if (n % 2 == 1) {
            stringBuilder.append(String.valueOf(x).repeat(n));
        } else {
            stringBuilder.append(String.valueOf(x).repeat(Math.max(0, n - 1)));
            stringBuilder.append(y);
        }
        return stringBuilder.toString();
    }

    public static int prefixCount(String[] words, String pref) {
        int count = 0;
        for (String word : words) {
            if (word.length() >= pref.length()) {
                String subs = word.substring(0, pref.length());
                if(subs.equals(pref)){
                    count++;
                }
            }

        }
        return count;
    }
    public static void reverseString(char[] s) {
        char[] reversed = new char[s.length];
        int counter = 0;
        for (int i = s.length -1; i >= 0 ; i--) {
            reversed[i] = s[counter];
            counter++;
        }
        for (int i = 0; i < s.length; i++) {
            s[i] = reversed[i];
        }
    }

    public static boolean areOccurrencesEqual(String s) {
        Map<Character, Integer> frequencyMap = new HashMap<>();
        for (char c : s.toCharArray()) {
            frequencyMap.put(c, frequencyMap.getOrDefault(c, 0) + 1);
        }
        int expectedFrequency = -1;
        for (int frequency : frequencyMap.values()) {
            if (expectedFrequency == -1) {
                expectedFrequency = frequency;
            } else if (expectedFrequency != frequency) {
                return false;
            }
        }

        return true;
    }

}
