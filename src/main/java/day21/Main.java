package day21;

import java.util.*;

public class Main {
    public static void main(String[] args){
    }
    public static int minOperations(int[] nums) {
        int count = 0;
        if(nums.length == 1){
            return 0;
        }
        for (int i = 1; i < nums.length ; i++) {
            if(nums[i] <= nums[i-1]){
                int increment = nums[i-1] - nums[i] + 1;
                nums[i] += increment;
                count += increment;
            }
        }
        return count;
    }
    public static int[] frequencySort(int[] nums) {
        Integer[] arr = new Integer[nums.length];
        Map<Integer, Integer> freq = new HashMap<>();
        for (int num: nums) {
            freq.put(num, freq.getOrDefault(num, 0)+1);
        }
        for (int i = 0; i < nums.length; i++) {
            arr[i] = nums[i];
        }
        Arrays.sort(arr, (a, b) ->{
            if(freq.get(a).equals(freq.get(b))){
                return Integer.compare(b, a);
            }
            return Integer.compare(freq.get(a), freq.get(b));
        });
        for (int i = 0; i < nums.length; i++) {
            nums[i] = arr[i];
        }
        return nums;
    }

    public static int minSteps(String s, String t) {
        int[] count = new int[26];
        for (int i = 0; i < s.length(); i++) {
            count[t.charAt(i) - 'a']++;
            count[s.charAt(i) - 'a']--;
        }
        int ans = 0;
        for (int i = 0; i < 26; i++) {
            ans += Math.max(0, count[i]);
        }
        return ans;
    }


}