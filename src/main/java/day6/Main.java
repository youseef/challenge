package day6;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        String[] names = sortPeople(new String[]{"Mary","John","Emma"}, new int[] {180,165,170});
        for (String name:
             names) {
            System.out.println(name);
        }
    }
    public static String makeSmallestPalindrome(String s) {
        StringBuilder str = new StringBuilder();
        char[] arr = new char[s.length()];
        int left = 0;
        int right = s.length() -1;
        while (left <= right){
            if(s.charAt(left) == s.charAt(right)){
                arr[left] = s.charAt(left);
                arr[right] = s.charAt(right);
            }else{
                arr[left] = arr[right] = (char)Math.min(s.charAt(left), s.charAt(right));
            }
            left++;
            right--;
        }
        for (int i = 0; i < arr.length; i++) {
            str.append(arr[i]);
        }
        return str.toString();
    }
    public static String replaceDigits(String s) {
        Map<Integer, Character> alphabet = new HashMap<>();
        for (int i = 1; i <= 26; i++) {
            char letter = (char) ('a' + i - 1);
            alphabet.put(i, letter);
        }
        StringBuilder result = new StringBuilder(s);
        for (int i = 0; i < s.length(); i++) {
            if (Character.isDigit(s.charAt(i))) {
                int chars = Integer.parseInt(Character.toString(s.charAt(i)));
                char ch;
                if (i - 1 < 0) {
                    ch = alphabet.get(chars);
                } else {
                    char prevChar = s.charAt(i - 1);
                    int key = getKeyByValue(alphabet, prevChar);
                    int finalKey = chars + key;
                    ch = alphabet.get(finalKey);
                }
                result.setCharAt(i, ch);
            }
        }
        return result.toString();
    }

    private static <K, V> K getKeyByValue(Map<K, V> map, V value) {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (value.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }
    public static int maximumNumberOfStringPairs(String[] words) {
        int count = 0;
        for (int i = 0; i < words.length; i++) {
            for (int j = i+1; j < words.length; j++) {
                StringBuilder str = new StringBuilder();
                str.append(words[j]);
                String reversed = str.reverse().toString();
                String actual = words[i];
                if(actual.equals(reversed)){
                    count++;
                    str.setLength(0);
                }else
                    str.setLength(0);
            }
        }
        return count;
    }
    public static int numOfStrings(String[] patterns, String word) {
        int count = 0;
        for (int i = 0; i < patterns.length; i++) {
            if(word.contains(patterns[i])){
                count++;
            }
        }
        return count;
    }
    public static String reversePrefix(String word, char ch) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < word.length(); i++) {
            if(word.charAt(i) == ch){
                StringBuilder str = new StringBuilder(word.substring(0, i+1));
                str.reverse();
                stringBuilder.append(word.substring(0, 0) + str.toString() + word.substring(i + 1, word.length()));
                break;
            }
        }
        return stringBuilder.toString();
    }
    public static String[] sortPeople(String[] names, int[] heights) {
        String[] sortedNames = new String[names.length];
        Map<Integer, String> heightsBy = new HashMap<>();
        for (int i = 0; i < names.length; i++) {
            heightsBy.put(heights[i], names[i]);
        }
        Set<Integer> keys = heightsBy.keySet();
        List<Integer> sortedKeys = new ArrayList<>(keys);
        Collections.sort(sortedKeys);
        int count = 0;
        for (int i = sortedNames.length-1; i >= 0; i--) {
            sortedNames[i] = heightsBy.get(sortedKeys.get(count));
            count++;
        }
        return sortedNames;
    }

    public static String destCity(List<List<String>> paths) {
        Set<String> startingCities = new HashSet<>();
        for (List<String> path : paths) {
            startingCities.add(path.get(0));
        }
        for (List<String> path: paths) {
            String destinyCity = path.get(1);
            if(!startingCities.contains(destinyCity)){
                return destinyCity;
            }
        }
        return null;
    }
}
