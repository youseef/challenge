package day20;
@FunctionalInterface
public interface AddingLambda {
    int add(int a, int b);

    default void sayHello(String name){
        System.out.println("Hello: "+ name);
    }
}
