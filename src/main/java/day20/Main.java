package day20;

import java.util.*;

public class Main {
    public static void main(String[] args){
        List<String> words = List.of("banana", "apple", "melon");
        words.stream().map(String::length).forEach(System.out::println);




    }

    public static List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> results = new ArrayList<>();
        Arrays.sort(nums);

        for (int i = 0; i < nums.length - 2; i++) {
            if (i > 0 && nums[i] == nums[i - 1]) continue;

            int left = i + 1;
            int right = nums.length - 1;

            while (left < right) {
                int sum = nums[i] + nums[left] + nums[right];

                if (sum == 0) {
                    results.add(Arrays.asList(nums[i], nums[left], nums[right]));
                    while (left < right && nums[left] == nums[left + 1]) left++;
                    while (left < right && nums[right] == nums[right - 1]) right--;

                    left++;
                    right--;
                } else if (sum < 0) {
                    left++;
                } else {
                    right--;
                }
            }
        }
        return results;
    }
    /*public static int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer>  validNumbers = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int compliment = target - nums[i];
            if(validNumbers.containsKey(compliment)){
                return  new int[]{validNumbers.get(compliment), i};
            }
            validNumbers.put(nums[i], i);
        }
        return new int[]{};
    }*/
    public static int findPermutationDifference(String s, String t) {
        int sum = 0;
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            sum += Math.abs(i - t.indexOf(ch));
        }
        return sum;
    }
    public static int scoreOfString(String s) {
        int sum = 0;
        for (int i = 0; i < s.length() - 1; i++) {
           sum += Math.abs((int)s.charAt(i) - (int)s.charAt(i + 1));
        }
        return sum;
    }
    public static void display(String name){
        System.out.println(name);
    }
    public static int display(int number){
        return number;
    }

    public static int balancedStringSplit(String s) {
        int count = 0;
        int balance = 0;
        for (char ch:s.toCharArray()) {
            if (ch == 'R')
                balance++;
            if(ch == 'L')
                balance--;
            if(balance == 0)
                count++;

        }
        return count;
    }
    public static String reverseWords(String s) {
        String[] words = s.split(" ");
        StringBuilder stringBuilder = new StringBuilder();
        for (String word: words) {
            StringBuilder str = new StringBuilder();
            for (int i = word.length()-1; i >= 0; i--) {
                str.append(word.charAt(i));
            }
            stringBuilder.append(str);
            stringBuilder.append(" ");
        }
        stringBuilder.delete(stringBuilder.length()-1, stringBuilder.length());
        return stringBuilder.toString();
    }

    public static Map<String, Integer> task(String word){
        Map<String, Integer> result = new HashMap<>();
        String[] words = word.split(" ");
        for (String wor:words) {
            wor = wor.toLowerCase();
            if (!result.containsKey(wor)){
                result.put(wor, 0);
            }
            result.put(wor, result.get(wor)+1);
        }
        return result;
    }

    public static int romanToInt(String s) {
        Map<Character, Integer> roman = new HashMap<>();
        roman.put('I', 1);
        roman.put('V', 5);
        roman.put('X', 10);
        roman.put('L', 50);
        roman.put('C', 100);
        roman.put('D', 500);
        roman.put('M', 1000);
        int total = 0;
        for (int i = 0; i < s.length(); i++) {
            if(i > 0 && roman.get(s.charAt(i)) >roman.get(s.charAt(i-1))){
                total += roman.get(s.charAt(i)) - 2 * roman.get(s.charAt(i - 1));
            }else
                total += roman.get(s.charAt(i));
        }
        return total;
    }

    public static String reverseWords2(String s) {
        StringBuilder str1 = new StringBuilder();
        StringBuilder str = new StringBuilder();
        s = s.trim();
        List<String> words = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i) != ' '){
                str1.append(s.charAt(i));
            }else {
                if (!str1.isEmpty()) {
                    words.add(str1.toString());
                    str1.setLength(0);
                }
            }
        }
        if (!str1.isEmpty()) {
            words.add(str1.toString());
        }
        for (int i = words.size()-1; i >= 1 ; i--) {
            str.append(words.get(i));
            str.append(" ");
        }
        str.append(words.get(0));
        return str.toString();
    }

    public static int binarySearch(int[] arr, int number){
        int left = 0;
        int right = arr.length -1;
        int numb = 0;
        while(left < right){
            int medium =left + (right - left)/2;
            if(arr[medium] > number){
                right = medium - 1;
            }
            if(arr[medium]< number){
                left = medium + 1;
            }else
                numb = medium;
        }
        return numb;
    }

    public static boolean hasDuplicate(int[] nums) {
        List<Integer> arr = new ArrayList<>();
        for (int num : nums) {
            if (arr.contains(num)) {
                return true;
            }
            arr.add(num);
        }
        return false;
    }
    public static boolean isAnagram(String s, String t) {
        s.toLowerCase();
        t.toLowerCase();
        char[] sArr = s.toCharArray();
        char[] tArr = t.toCharArray();

        Arrays.sort(sArr);
        Arrays.sort(tArr);
        return Arrays.equals(sArr, tArr);
    }

    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> valid = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int compliment = target - nums[i];
            if(valid.containsKey(compliment)){
                return new int[]{valid.get(compliment), i};
            }
            valid.put(nums[i], i);
        }
        return new int[]{};
    }

}
