package day2;

import java.util.Arrays;

public class Main {
    public static void main(String[] arg){
        int[] arr = numberGame(new int[]{5,4,2,3});
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }
    public static int[] decode(int[] encoded, int first) {
        int[] arr = new int[encoded.length + 1];
        arr[0] = first;
        int mysteriousNumber = 0;
        for (int i = 0; i < encoded.length; i++) {
            while(mysteriousNumber < Integer.MAX_VALUE){
                if((arr[i] ^ mysteriousNumber) == encoded[i]){
                    arr[i+1] = mysteriousNumber;
                    mysteriousNumber = 0;
                    break;
                }
                mysteriousNumber++;
            }
        }
        return arr;
    }

    public static int[] leftRightDifference(int[] nums) {
        int[] results = new int[nums.length];
        int[] leftNums = new int[nums.length];
        int[] rightNums = new int[nums.length];
        int summedUpNumber = 0;
        int summedNumber = 0;
        for (int i = 0; i < leftNums.length-1; i++) {
            summedUpNumber += nums[i];
            leftNums[i+1] = summedUpNumber;

        }
        for (int i = rightNums.length-1; i > 0; i--) {
            summedNumber += nums[i];
            rightNums[i-1] = summedNumber;
        }

        for (int i = 0; i < results.length; i++) {
            results[i] = Math.abs(leftNums[i] - rightNums[i]);
        }

        return results;
    }

    public static int[] createTargetArray(int[] nums, int[] index) {
        int[] resultArray = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            int currentIndex = index[i];
            // Shift elements to the right to make space for the new element
            for (int j = nums.length - 1; j > currentIndex; j--) {
                resultArray[j] = resultArray[j - 1];
            }
            // Insert the new element at the correct position
            resultArray[currentIndex] = nums[i];
        }
        return resultArray;
    }
    public static int[] numberGame(int[] nums) {
        int[] resultArray = new int[nums.length];
        Arrays.sort(nums);
        int[] Bob = new int[nums.length/2];
        int[] Alice = new int[nums.length/2];
        int bobCounter = 0;
        int AliceCounter = 0;
        for (int i = 0; i < nums.length; i++) {
            if(i%2 == 0){
                Alice[AliceCounter] = nums[i];
                AliceCounter++;
            }else{
                Bob[bobCounter] = nums[i];
                bobCounter++;
            }
        }
        AliceCounter = 0;
        bobCounter = 0;
        for (int i = 0; i < resultArray.length; i++) {
            if(i%2 == 0){
                resultArray[i] = Bob[bobCounter];
                bobCounter++;
            }else {
                resultArray[i] = Alice[AliceCounter];
                AliceCounter++;
            }
        }
        return resultArray;
    }
}
