package day8;

import java.net.Inet4Address;
import java.util.*;

public class Main {
    public static void main(String[] arg){
        System.out.println(sumOfSquares(new int[]{2,7,1,19,18,3}));
    }

    public static int removePalindromeSub(String s) {
        if(s.isEmpty() || isPalindrome(s)){
            return 1;
        }else
            return 2;
    }

    private static boolean isPalindrome(String s) {
        boolean flag = true;
        int left = 0;
        int right = s.length() - 1;
        while (left < right){
            if(s.charAt(left) != s.charAt(right)){
                flag = false;
            }
            right--;
            left++;
        }
        return flag;

    }

    public static int countSeniors(String[] details) {
        int count = 0;
        for (String citizen: details) {
            String age = citizen.substring(11, 13);
            int ageInNumber = Integer.parseInt(age);
            if(ageInNumber > 60){
                count++;
            }
        }
        return count;
    }
    public static boolean judgeCircle(String moves) {
        int leftAndRight = 0;
        int upAndDown = 0;

        for (char ch : moves.toCharArray()) {
            if (ch == 'L') {
                leftAndRight--;
            } else if (ch == 'R') {
                leftAndRight++;
            } else if (ch == 'U') {
                upAndDown++;
            } else if (ch == 'D') {
                upAndDown--;
            }
        }

        return leftAndRight == 0 && upAndDown == 0;
    }

    public static int minimizedStringLength(String s) {
        Set<Character> uniqueChar = new HashSet<>();
        StringBuilder stringBuilder = new StringBuilder();
        for (char ch: s.toCharArray()) {
            if(uniqueChar.add(ch)){
                stringBuilder.append(ch);
            }
        }
        return stringBuilder.length();
    }
    public static int minDeletionSize(String[] strs) {
        int deleted = 0;
        int n = strs[0].length();

        for (int col = 0; col < n; col++) {
            for (int row = 1; row < strs.length; row++) {
                if (strs[row].charAt(col) < strs[row - 1].charAt(col)) {
                    deleted++;
                    break;
                }
            }
        }

        return deleted;
    }

    public static int diagonalSum(int[][] mat) {
        int sum = 0;
        for (int i = 0; i < mat.length; i++) {
            sum += mat[i][i];
        }
        int index = 0;
        for (int i = mat[0].length - 1; i >= 0 ; i--) {
            sum += mat[index][i];
            index++;
        }
        if(mat.length%2 == 1) {
            sum -= mat[mat.length / 2][mat.length / 2];
        }
        return sum;
    }

    public static int sumOfSquares(int[] nums) {
        int sum = 0;
        List<Integer> numList = new ArrayList<>();
        numList.add(0);
        for (int i = 0; i < nums.length; i++) {
            numList.add(nums[i]);
        }

        int length = nums.length;
        for (int i = 1; i <= length; i++) {
            if(length % i == 0){
                sum += (numList.get(i) * numList.get(i));
            }
        }
        return sum;
    }

    public static int maxProduct(int[] nums) {
        Arrays.sort(nums);
        return (nums[nums.length -1] -1) * (nums[nums.length -2] -1);
    }

    
}
