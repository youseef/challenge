package day13;

import java.net.Inet4Address;
import java.util.*;

public class Main {
    public static void main(String[] args){
        System.out.println(customSortString("cba", "abcd"));
    }
    public static int countOperations(int num1, int num2) {
        int numberOfOperations = 0;
        if(num1 == num2){
            return 1;
        }
        while (true){
            if(num1 == 0 || num2 == 0){
                break;
            }
            if(num1 > num2){
                num1 -=num2;
            }else
                num2 -= num1;

            numberOfOperations++;
        }
        return numberOfOperations;
    }
    public static int distributeCandies(int n, int limit) {
        int count = 0;
        for (int i = 0; i <= limit; i++) {
            for (int j = 0; j <= limit; j++) {
                for (int k = 0; k <= limit; k++) {
                    if((i + j + k == n && (i <= limit) && (j <= limit) && (k <= limit))){
                        count++;
                    }
                }
            }
        }
        return count;
    }

    public static int countSymmetricIntegers(int low, int high) {
        int count = 0;
        for (int i = low; i <= high ; i++) {
            String num = Integer.toString(i);
            if(num.length() % 2 == 0){
                String part1 = num.substring(0, num.length()/2);
                String part2 = num.substring(num.length()/2);
                int sum1 = 0;
                int sum2 = 0;
                for (char ch: part1.toCharArray()) {
                    int digit = ch -'0';
                    sum1 += digit;
                }
                for (char ch: part2.toCharArray()) {
                    int digit = ch -'0';
                    sum2 += digit;
                }
                if(sum1 == sum2){
                    count++;
                }
            }
        }
        return count;
    }
    public static int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> intersections = new HashSet<>();
        for (int i = 0; i < nums1.length; i++) {
            for (int j = 0; j < nums2.length; j++) {
                if(nums1[i] == nums2[j] && !intersections.contains(nums1[i])){
                    intersections.add(nums1[i]);
                }
            }
        }
        int[] resultarr = new int[intersections.size()];
        int index = 0;
        for (int num: intersections) {
            resultarr[index] = num;
            index++;
        }
        return resultarr;
    }
    public static int[] buildArray(int[] nums) {
        int[] ans = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            ans[i] = nums[nums[i]];
        }
        return ans;
    }
    public static int[] separateDigits(int[] nums) {
        List<Integer> numbers = new ArrayList<>();
        for (int num : nums) {
            List<Integer> temporary = new ArrayList<>();
            while (num > 0){
                int digit = num%10;
                temporary.add(digit);
                num /= 10;
            }
            for (int i = temporary.size()-1; i >= 0; i--) {
                numbers.add(temporary.get(i));
            }
            temporary.clear();
        }
        int[] ans = new int[numbers.size()];
        for (int i = 0; i < ans.length; i++) {
            ans[i] = numbers.get(i);
        }
        return ans;
    }
    public static String customSortString(String order, String s) {
        StringBuilder stringBuilder = new StringBuilder();
        List<Character> ordered = new ArrayList<>();
        for (int i = 0; i < order.length(); i++) {
            for (int j = 0; j < s.length(); j++) {
                if(order.charAt(i) == s.charAt(j)){
                    ordered.add(order.charAt(i));
                }
            }
        }
        for (char ch: ordered) {
            stringBuilder.append(ch);
        }
        StringBuilder stringBuilder1 = new StringBuilder();
        for (char ch:s.toCharArray()) {
            if(stringBuilder.toString().indexOf(ch) == -1){
                stringBuilder1.append(ch);
            }
        }
        return stringBuilder.append(stringBuilder1).toString();
    }

}
