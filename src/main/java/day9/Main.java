package day9;

import java.util.Arrays;

public class Main {
    public static void main(String[] arg){
        System.out.println(convertTemperature(36.50));
    }
    public static int theMaximumAchievableX(int num, int t) {
        return num + (t * 2);
    }

    public static double[] convertTemperature(double celsius) {
        double kelvin = celsius + 273.15;
        double fahrenheit = celsius * 1.8 + 32;

        double[] result = new double[2];
        result[0] = roundToDecimalPlaces(kelvin, 5);
        result[1] = roundToDecimalPlaces(fahrenheit, 5);

        return result;
    }

    public static double roundToDecimalPlaces(double number, int decimalPlaces) {
        double powerOfTen = Math.pow(10, decimalPlaces);
        return Math.round(number * powerOfTen) / powerOfTen;
    }
    public int differenceOfSums(int n, int m) {
        int num1 = 0;
        int num2 = 0;
        for (int i = 0; i <= n; i++) {
            if(i%m == 0){
                num2 += i;
            }else {
                num1 += i;
            }
        }
        return num1 -num2;
    }
    public int sum(int num1, int num2) {
        return num1 + num2;
    }
    public static int smallestEvenMultiple(int n) {
        int num = 0;
        for (int i = 1; i < Integer.MAX_VALUE; i++) {
            if(i % n == 0 && i%2 == 0){
                num = i;
                break;
            }
        }
        return num;
    }
    public static int subtractProductAndSum(int n) {
        int multiplied = 1;
        int added = 0;
        String numbers = Integer.toString(n);
        char[] number = numbers.toCharArray();
        for (int i = 0; i < number.length; i++) {
            int j = number[i] -'0';
            multiplied *= j;
            added += j;
        }
        return multiplied - added;
    }

    public static int numberOfMatches(int n) {
        return n - 1;
    }
    public static int numberOfSteps(int num) {
        int steps = 0;
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            if(num % 2 == 0){
                num = num/2;
                steps++;
            }else {
                num = num-1;
                steps++;
            }
            if(num == 0){
                break;
            }
        }
        return steps;
    }

    public static int sumOfMultiples(int n) {
        int sum = 0;
        for(int i = 0; i <= n; i++){
            if(i%3 ==0 || i%5 == 0|| i%7 == 0){
                sum += i;
            }
        }
        return sum;
    }
    public static int countDigits(int num) {
        int count = 0;
        String nums = Integer.toString(num);
        char[] arr = nums.toCharArray();
        for (int i = 0; i < arr.length; i++) {
            int n = arr[i] - '0';
            if(num %n == 0){
                count++;
            }
        }
        return count;
    }
    public static int deleteGreatestValue(int[][] grid) {
        int[] rowMax = new int [grid.length];
        Arrays.fill(rowMax,Integer.MIN_VALUE);
        int ans= 0;
        int collength = grid[0].length;

        while(collength>0){
            for (int i = 0; i < grid.length; i++) {
                for (int j = 0; j < grid[0].length; j++) {
                    rowMax[i] = Math.max(grid[i][j],rowMax[i]);
                }
            }
            ans = ans + Arrays.stream(rowMax).max().getAsInt();
            for (int i = 0; i <grid.length ; i++) {
                for (int j = 0; j < grid[0].length; j++) {
                    if(grid[i][j]== rowMax[i]) {
                        grid[i][j]= Integer.MIN_VALUE;
                        rowMax[i]= Integer.MIN_VALUE;
                    }
                }
            }
            collength--;
        }
        return ans;
    }
}
