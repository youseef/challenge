package day5;

import java.util.*;

public class Main {
    public static void main(String[] arg){
        System.out.println(finalString("string"));
    }
    public static int countConsistentStrings(String allowed, String[] words) {
        int count = 0;
        for (String word: words) {
            if(isConsistent(allowed, word)){
                count++;
            }
        }

        return count;
    }

    private static boolean isConsistent(String allowed, String word) {
        for (char ch: word.toCharArray()) {
            if(allowed.indexOf(ch) == -1){
                return false;
            }
        }
        return true;
    }

    public static int largestAltitude(int[] gains) {
        int currentAltitude = 0;
        int highestAltitude = currentAltitude;
        for (int gain: gains) {
            currentAltitude += gain;

            highestAltitude = Math.max(currentAltitude, highestAltitude);
        }
        return highestAltitude;

    }
    public static int maxProductDifference(int[] nums) {
        int maximized = 0;
        Arrays.sort(nums);
        int x = nums[0];
        int y = nums[1];
        int w = nums[nums.length -1];
        int z = nums[nums.length - 2];

        maximized = (w * z) - (x * y);
        return maximized;
    }
    public static boolean isAcronym(List<String> words, String s) {
        if(words.size() != s.length()){
            return false;
        }
        int counter = 0;
        for (String word: words){
            if(word.charAt(0) != s.charAt(counter)){
                return false;
            }
            counter++;
        }
        return true;
    }
    public static String toLowerCase(String s) {
        return s.toLowerCase();
    }


    public static boolean checkIfPangram(String sentence) {
        Set<Character> alphabet = new HashSet<>();
        for (char ch: sentence.toCharArray()) {
            if(Character.isLetter(ch)){
                alphabet.add(ch);
            }
        }
        return alphabet.size() == 26;
    }
    public static String sortSentence(String s) {
        String[] str = s.split(" ");
        String[] res = new String[str.length];
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (String elem : str) {
            i = (int) (elem.charAt(elem.length() - 1) - '0');
            res[i - 1] = elem.substring(0, elem.length() - 1);
        }
        for (i = 0; i < res.length - 1; i++)
            sb.append(res[i]).append(" ");
        sb.append(res[i]);
        return sb.toString();
    }
    public static String interpret(String command) {
        StringBuilder stringBuilder = new StringBuilder();
        int index = 0;
        while (index < command.length()){
            if(command.startsWith("G", index)){
                stringBuilder.append("G");
                index +=1;
            }else if(command.startsWith("()", index)){
                stringBuilder.append("o");
                index += 2;
            }else if(command.startsWith("(al)", index)){
                stringBuilder.append("al");
                index += 4;
            }
        }
        return stringBuilder.toString();
    }

    public static String finalString(String s) {
        StringBuilder stringBuilder = new StringBuilder();
        for (char ch: s.toCharArray()) {
            if(ch == 'i'){
                stringBuilder.reverse();
            }else
                stringBuilder.append(ch);
        }
        return stringBuilder.toString();
    }

}
