package day14;

import javax.swing.*;
import java.util.*;

public class Main {
    public static void main(String[] args){
    }
    public static String kthDistinct(String[] arr, int k) {
        List<String> stored = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            for (int j = i+1; j < arr.length - 1; j++) {
                if(!Objects.equals(arr[i], arr[j]) && !stored.contains(arr[i])){
                    stored.add(arr[i]);
                }
            }
        }
        if(k > stored.size()){
            return " ";
        }else
            return stored.get(k-1);
    }

    public static String longestCommonPrefix(String[] strs) {
        if(strs.length == 0)
            return "";
        String prefix = strs[0];
        for(int i = 1; i<strs.length; i++){
            while(strs[i].indexOf(prefix) != 0){
                prefix = prefix.substring(0, prefix.length()-1);
                if(prefix.isEmpty())
                    return "";
            }
        }
        return prefix;
    }
    public static int numSubarraysWithSum(int[] nums, int goal) {
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            int sum = 0;
            for (int j = i; j < nums.length; j++) {
                sum += nums[j];
                if (sum == goal) {
                    count++;
                }
            }
        }

        return count;
    }
    public static int sumOfUnique(int[] nums) {
        Map<Integer, Integer> unique = new HashMap<>();
        for (int numbers: nums) {
            unique.put(numbers, unique.getOrDefault(numbers, 0)+1);
        }
        int sum = 0;
        for (Map.Entry<Integer, Integer> entry: unique.entrySet()) {
            if(entry.getValue() == 1){
                sum += entry.getKey();
            }
        }
        return sum;
    }
    public int findNumbers(int[] nums) {
        int count = 0;
        for (int number: nums) {
            String num = Integer.toString(number);
            if(num.length() %2 ==0){
                count++;
            }
        }
        return count;
    }
    public static boolean uniqueOccurrences(int[] arr) {
        Map<Integer, Integer> countMap = new HashMap<>();

        // Count occurrences of each number
        for (int num : arr) {
            countMap.put(num, countMap.getOrDefault(num, 0) + 1);
        }

        Set<Integer> occurrences = new HashSet<>();

        // Check if counts are unique
        for (int count : countMap.values()) {
            if (!occurrences.add(count)) {
                return false; // If count is not unique, return false
            }
        }

        return true;
    }
}
