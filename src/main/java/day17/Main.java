package day17;

import java.util.List;

public class Main {
    public static void main(String[] args){
        System.out.println(minimumTotal(List.of(List.of(-1),List.of(2,3),List.of(1, -1, -3))));
    }
    public static int minimumTotal(List<List<Integer>> triangle) {
        int minPath = 0;
        int place = 0;
        for (List<Integer> miniArr: triangle) {
            int number = Integer.MAX_VALUE;
            for (int i = place; i < miniArr.size(); i++) {
                number = Math.min(miniArr.get(i), number);
                if(number == miniArr.get(i)){
                    place = i;
                }
            }
            minPath += number;
        }
        return minPath;
    }
}
