package day25;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        System.out.println(removeDuplicateLetters("cbacdcbc"));
    }
    private static class LargeNumberComparator implements Comparator<String> {
        @Override
        public int compare(String o1, String o2) {
            String order1 = o1 + o2;
            String order2 = o2 + o1;
            return order2.compareTo(order1);
        }

    }
    public static String largestNumber(int[] nums) {
        StringBuilder sb = new StringBuilder();
        String[] numbers = new String[nums.length];
        for (int i = 0; i < nums.length; i++) {
            numbers[i] = String.valueOf(nums[i]);
        }
        Arrays.sort(numbers, new LargeNumberComparator());

        if(numbers[0] == "0"){
            return "0";
        }
        for (int i = 0; i < numbers.length; i++) {
            sb.append(numbers[i]);
        }
        return sb.toString();
    }
    public static int countPrimes(int n) {
        Set<Integer> primes = new HashSet<>();
        for (int i = 0; i < n; i++) {
            if (isPrime(i)) {
                primes.add(i);
            }
        }
        return primes.size();
    }
    public static boolean isPrime(int n) {
        int i = 2;
        if(n ==0 || n == 1){
            return false;
        }
        if(n == i){
            return true;
        }
        if(n % i == 0){
            return false;
        }
        i++;
        return isPrime(n);
    }

    public static String removeDuplicateLetters(String s) {
        int[] lastOccurrences = new int[26];
        boolean[] inStack = new boolean[26];
        Stack<Character> stack = new Stack<>();

        for (int i = 0; i < s.length(); i++) {
            lastOccurrences[s.charAt(i) - 'a'] = i;
        }
        for (int i = 0; i < s.length(); i++) {
            char current = s.charAt(i);

            if(inStack[current-'a']) continue;

            while (!stack.isEmpty() && stack.peek() > current && lastOccurrences[stack.peek()-'a'] > i){
                inStack[stack.pop()-'a'] = false;
            }
            stack.push(current);
            inStack[current-'a'] = true;
        }
        StringBuilder sb = new StringBuilder();
        for(char c: stack){
            sb.append(c);
        }
        return sb.toString();
    }
}
