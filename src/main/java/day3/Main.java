package day3;

import java.util.*;

public class Main {
    public static void main(String[] arg){
        System.out.println(differenceOfSum(new int[]{1,1649}));
    }
    public static int[] decompressRLElist(int[] nums) {
        int arrSize = 0;
        for (int i = 0; i < nums.length; i += 2) {
            arrSize += nums[i];
        }

        int[] result = new int[arrSize];

        int startIdx = 0;
        for (int i = 0; i < nums.length; i += 2) {
            Arrays.fill(result, startIdx, startIdx + nums[i], nums[i + 1]);
            startIdx += nums[i];
        }
        return result;
    }
    public static boolean arrayStringsAreEqual(String[] word1, String[] word2) {
        String word = "";
        String words = "";
        for (int i = 0; i < word1.length; i++) {
            word += word1[i];
        }
        for (int i = 0; i < word2.length; i++) {
            words += word2[i];
        }
        return word.equals(words);
    }
    public static int countMatches(List<List<String>> items, String ruleKey, String ruleValue) {
        int count = 0;
        for (List<String> item: items) {
            HashMap<String, String> listWithKeys = new HashMap<>();
            if(item.size() <= 3){
                listWithKeys.put("type", item.get(0));
                listWithKeys.put("color", item.get(1));
                listWithKeys.put("name", item.get(2));
                for (Map.Entry<String, String> entry : listWithKeys.entrySet()){
                    if(Objects.equals(entry.getKey(), ruleKey) && Objects.equals(entry.getValue(), ruleValue)){
                        count++;
                    }
                }
                listWithKeys.clear();
            }
        }
        return count;
    }
    public static String restoreString(String s, int[] indices) {
        StringBuilder stringBuilder = new StringBuilder();
        Map<Integer, Character> wordMap = new HashMap<>();
        char[] arr = s.toCharArray();
        for (int i = 0; i <indices.length ; i++) {
            wordMap.put(indices[i], arr[i]);
        }
        List<Integer> sortedKeys = new ArrayList<>(wordMap.keySet());
        Collections.sort(sortedKeys);

        for (Integer key: sortedKeys) {
            Character value = wordMap.get(key);
            stringBuilder.append(value);
        }
        return stringBuilder.toString();
    }
    public static String truncateSentence(String s, int k) {
        StringBuilder stringBuilder = new StringBuilder();
        int counter = 0;
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == ' '){
                counter++;
            }
            if(counter < k){
                stringBuilder.append(s.charAt(i));
            }
        }
        return stringBuilder.toString();
    }
    public static String firstPalindrome(String[] words) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < words.length; i++) {
            String word = words[i];
            int right = word.length()-1;
            int left = 0;
            int counter = 0;
            while (left < right){
                if(word.charAt(left) == word.charAt(right)){
                    counter++;
                }
                if(word.length()/2 == counter){
                    stringBuilder.append(word);
                }
                right--;
                left++;
            }
            if(!stringBuilder.isEmpty()){
                break;
            }
        }
        return stringBuilder.toString();
    }
    public static int differenceOfSum(int[] nums) {
        int sum = 0;
        int sum2 = 0;
        for (int i = 0; i < nums.length; i++) {
            int n = nums[i];
            if(n >= 10 && n <= 99){
                int f = n/10;
                int m = n%10;
                n = m+f;
            } else if (n >= 100 && n <= 999) {
                int f = n/100;
                int m = n%10;
                int q = (n - 100 * f - m)/10;
                n = f + m + q;
            }else if(n >= 1000){
                int f = n / 1000;
                int m = n%10;
                int q = (n - 1000 * f - m)/100;
                int p =((n - 1000 * f - m)%100)/10;
                n = f + m + p + q;
            }
            sum += n;
            sum2 += nums[i];
        }
        return Math.abs(sum2 - sum);
    }
}
