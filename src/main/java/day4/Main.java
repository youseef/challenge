package day4;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] arg){
        System.out.println(countKDifference(new int[]{1,2,2,1}, 1));
    }
    public static int arithmeticTriplets(int[] nums, int diff) {
        Set<Integer> set = new HashSet<>();
        int i = 0, k = 1, result = 0;

        for(int num : nums)
            set.add(num);

        while(k < nums.length) {
            if(nums[k] - nums[i] == 2 * diff && set.contains(diff + nums[i]))
                ++result;

            if(nums[k] - nums[i] < 2 * diff)
                ++k;
            else
                ++i;
        }

        return result;
    }
    public static int countKDifference(int[] nums, int k) {
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            for (int j = i; j < nums.length; j++) {
                if(Math.abs(nums[i] - nums[j]) == k){
                    count++;
                }
            }
        }
        return count;
    }
    public static int sumOddLengthSubarrays(int[] arr) {
        int windowSize = 1;
        int currentSum = 0;
        int finalSum = 0;

        for (windowSize = 1; windowSize <= arr.length ; windowSize+=2) {
            currentSum = sumOfAllSlidingWindowOfSizeK(arr , windowSize);
            finalSum += currentSum;
        }

        return finalSum;
    }
    public static int sumOfAllSlidingWindowOfSizeK(int[] arr , int k ) {
        int currSum = 0;
        int absSum = 0;
        //[1,]
        for (int j = 0; j < k; j++) {
            currSum += arr[j];
        }
        absSum += currSum;
        for (int j = k; j < j + k && j < arr.length; j++) {
            currSum = currSum - arr[j - k] + arr[j];
            absSum += currSum;
        }
        return absSum;
    }
}
