package day24;

import java.math.BigInteger;
import java.util.Map;
import java.util.Stack;

public class Main {
    public static void main(String[] args){
        System.out.println(myPow(2.00000, -2147483648));
    }

    private static final  String[] belowTen = {"", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};
    private static final String[] belowHundred = {"", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};
    private static final String[] belowTwenty = { "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"};
    public static String numberToWords(int num) {
        if(num == 0){
            return "Zero";
        }
        return convertToWords(num);
    }

    private static String convertToWords(int num) {
        if(num < 10){
            return belowTen[num];
        }
        if(num < 20){
            return belowTwenty[num - 10];
        }
        if(num < 100){
            return belowHundred[num/10] + (num%10 != 0?" " + convertToWords(num%10):"");
        }
        if (num < 1000) {
            return convertToWords(num / 100) + " Hundred" + (num % 100 != 0 ? " " + convertToWords(num % 100) : "");
        }
        if (num < 1000000) {
            return convertToWords(num / 1000) + " Thousand" + (num % 1000 != 0 ? " " + convertToWords(num % 1000) : "");
        }
        if (num < 1000000000) {
            return convertToWords(num / 1000000) + " Million" + (num % 1000000 != 0 ? " " + convertToWords(num % 1000000) : "");
        }
        return convertToWords(num / 1000000000) + " Billion" + (num % 1000000000 != 0 ? " " + convertToWords(num % 1000000000) : "");
    }
    public static int superPow(int a, int[] b) {
        StringBuilder s = new StringBuilder();
        for (int n: b) {
            s.append(n);
        }
        long result = 1;
        int bNumber =Integer.parseInt( s.toString());
        for (int i = 1; i <= bNumber; i++) {
            result *= a;
        }

        return (int) (result % 1337);
    }
    public static String decodeString(String s){
        Stack<Integer> stack = new Stack<>();
        Stack<StringBuilder> stack1 = new Stack<>();
        StringBuilder str = new StringBuilder();
        int n = 0;

        for (char c: s.toCharArray()){
            if(Character.isDigit(c)){
                n = n * 10 + (c - '0');
            } else if (c == '[') {
                stack.push(n);
                n = 0;
                stack1.push(str);
                str = new StringBuilder();
            }else if(c == ']'){
                int k = stack.pop();
                StringBuilder temp = str;
                str = stack1.pop();
                while (k-- > 0){
                    str.append(temp);
                }
            }else
                str.append(c);
        }
        return str.toString();
    }

    public static double myPow(double x, int n) {
        double result = 1.0;
        if(x == 1.0){
            return result;
        }
        if(n == Integer.MAX_VALUE && x == -1.0){
            return -1.0;
        }
        if(n == Integer.MIN_VALUE && x == -1.0){
            return 1.0;
        }
        if(n == Integer.MIN_VALUE){
            return 0.0;
        }
        for (int i = 0; i < Math.abs(n); i++) {
            result *= x;
        }
        if(n < 0){
            return 1 / result;
        }else
            return result;

    }
}
