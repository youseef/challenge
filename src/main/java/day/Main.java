package day;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] arg){
        System.out.println(sumIndicesWithKSetBits(List.of(5,10,1,5,2), 1));
    }

    public static int[] archive(int[] data){
        int[] archivedData = new int[data.length * 2];

        for (int i = 0; i < archivedData.length; i++) {
            if(i < data.length){
                archivedData[i] = data[i];
            }
            else {
                for (int j = 0; j < data.length; j++) {
                    archivedData[i] = data[j];
                    i++;
                }
            }
        }
        return archivedData;
    }
    public static int[] buildArray(int[] nums) {
        Arrays.sort(nums);
        return nums;
    }

    public static int numIdenticalPairs(int[] nums) {
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if(nums[i] == nums[j]){
                    count++;
                }
            }
        }
        return count;
    }

    public static int[] shuffle(int[] nums, int n) {
        int[] arr = new int[nums.length];
        int count = 0;
        int count1 = 0;
        for (int i = 0; i < nums.length; i++) {
            if(i%2 == 0){
                arr[i] = nums[count1];
                count1 +=1;
            } else if (i%2 == 1){
                arr[i] = nums[count+n];
                count++;
            }

        }
        return arr;
    }
    public static int maximumWealth(int[][] accounts) {
        List<Integer> wealth = new ArrayList<>();
        int r = accounts.length;
        int c = accounts[0].length;

        int eachCostumerWealth = 0;

        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                eachCostumerWealth += accounts[i][j];
            }
            wealth.add(eachCostumerWealth);
            eachCostumerWealth = 0;
        }
        return wealth.stream().max(Integer::compareTo).orElse(0);
    }

    public static int numberOfEmployeesWhoMetTarget(int[] hours, int target) {
        int employeesRate = 0;
        for (int i = 0; i < hours.length; i++) {
            if(hours[i] >= target){
                employeesRate++;
            }
        }
        return employeesRate;
    }

    public static List<Boolean> kidsWithCandies(int[] candies, int extraCandies) {
        List<Boolean> results = new ArrayList<>();
        int highestNumber = Arrays.stream(candies).max().orElse(0);
        for (int i = 0; i < candies.length; i++) {
            int numb = candies[i] + extraCandies;
            if(numb > highestNumber){
                results.add(true);
            }else
                results.add(false);
        }
        return results;
    }
    public static int countPairs(List<Integer> nums, int target) {
        int counts = 0;
        for (int i = 0; i < nums.size(); i++) {
            for (int j = i+1; j < nums.size(); j++) {
                if(nums.get(i) + nums.get(j) < target){
                    counts++;
                }
            }
        }
        return counts;
    }
    public static int[] runningSum(int[] nums) {
        int number = 0;
        int[] arr = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            number += nums[i];
            arr[i] = number;
        }
        return arr;
    }
    public static int[] smallerNumbersThanCurrent(int[] nums) {
        int[] arr = new int[nums.length];
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums.length; j++) {
                if(nums[i] > nums[j]){
                    count++;
                }
            }
            arr[i] = count;
            count = 0;
        }
        return arr;
    }
    public static int mostWordsFound(String[] sentences) {
        List<Integer> words = new ArrayList<>();
        int count = 0;
        for (int i = 0; i < sentences.length; i++) {
            String sentence = sentences[i];
            char[] arr = sentence.toCharArray();
            for (int j = 0; j < arr.length; j++) {
                if(arr[j] == ' '){
                    count++;
                }
            }
            count++;
            words.add(count);
            count = 0;
        }
        return words.stream().max(Integer::compareTo).orElse(0);
    }
    public static int sumIndicesWithKSetBits(List<Integer> nums, int k) {
        int sum = 0;
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilder1 = new StringBuilder();
        for (int j = 0; j < k; j++) {
            stringBuilder.append("1");
        }
        for (int i = 0; i < nums.size(); i++) {
            String binaryRepresentationOfNumber = Integer.toBinaryString(i);
            for (int j = 0; j < binaryRepresentationOfNumber.length(); j++) {
                if(binaryRepresentationOfNumber.charAt(j) != '0'){
                    stringBuilder1.append(binaryRepresentationOfNumber.charAt(j));
                }
            }
            if(stringBuilder1.toString().equals(stringBuilder.toString())){
                sum += nums.get(i);

            }
            stringBuilder1.setLength(0);
        }
        return sum;
    }
}
