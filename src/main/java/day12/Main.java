package day12;

import java.net.Inet4Address;
import java.util.*;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] arg){
        //System.out.println(Arrays.toString(findMissingAndRepeatedValues(new int[][]{{9, 1, 7}, {8, 9, 2}, {3, 4, 6}})));
        System.out.println(Arrays.toString(findMissingAndRepeatedValues(new int[][]{{1, 2}, {1, 4}})));
    }
    public static boolean isSameAfterReversals(int num) {
        String initialNumber = Integer.toString(num);
        int reversed1 = Integer.parseInt(reverse(initialNumber));
        String reversed1AsString = Integer.toString(reversed1);
        int reversed2 = Integer.parseInt(reverse(reversed1AsString));
        return num == reversed2;
    }

    private static String reverse(String initialNumber) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = initialNumber.length() - 1; i >= 0 ; i--) {
            stringBuilder.append(initialNumber.charAt(i));
        }
        return stringBuilder.toString();
    }
    public static int commonFactors(int a, int b) {
        int max = Math.max(a, b);
        int count =0;
        for (int i = 1; i <= max; i++) {
            if(a % i == 0 && b%i == 0){
                count++;
            }
        }
        return count;
    }
    public static int findDelayedArrivalTime(int arrivalTime, int delayedTime) {
        if(arrivalTime == 24){
            arrivalTime = 0;
        }
        if(delayedTime == 24){
            delayedTime = 0;
        }
        int arrival = arrivalTime + delayedTime;
        if(arrival >= 24){
            return arrival -24;
        }else
            return arrival;
    }

    public static List<Integer> selfDividingNumbers(int left, int right) {
        List<Integer> numbers = new ArrayList<>();
        for (int i = left; i <= right; i++) {
            int number = i;
            if(selfdivide(number)){
                numbers.add(number);
            }
        }
        return numbers;
    }

    private static boolean selfdivide(int number) {
        boolean flag = true;
        String num = Integer.toString(number);
        if(num.contains("0"))
            return false;
        for (int i = 0; i < num.length(); i++) {
            int n = num.charAt(i) - '0';
            if(number%n !=0){
                flag = false;
                break;
            }
        }
        return flag;
    }
    public static int findGCD(int[] nums) {
        int min = Arrays.stream(nums).min().getAsInt();
        int max = Arrays.stream(nums).max().getAsInt();
        int number = 0;

        for (int i = 1; i <= max; i++) {
            if(min%i == 0 && max%i == 0){
                number = i;
            }
        }
        return number;
    }
    public static int pivotInteger(int n) {
        if(n == 1){
            return 1;
        }
        int leftSum = 0;
        int rightSum = IntStream.range(0,n+1).sum();
        int pivot = -1;
        for (int i = 0; i < n; i++) {
            leftSum += i;
            if(leftSum == rightSum){
                pivot = i;
            }
            rightSum -= i;
        }
        return pivot;
    }
    public static int[] sumZero(int n) {
        int[] nums = new int[n];
        int num = -1;
        int num1 = -1;
        if(n%2 == 0){
            int n1 = 1;
            for (int i = 0; i < n/2; i++) {
                nums[i] = n1;
                n1++;
            }
            for (int i = n/2; i < n; i++) {
                nums[i] = num;
                num--;
            }
        }else{
            for (int i = 0; i <= n/2; i++) {
                nums[i] = i;
            }
            for (int i = n/2 + 1; i < n; i++) {
                nums[i] = num1;
                num1--;
            }
        }
        return nums;
    }
    public static int[] findMissingAndRepeatedValues(int[][] grid) {
        int[] result = new int[2];
        int length = grid.length;
        int[] arr = new int[grid.length * grid.length];
        int count = 0;

        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                arr[count] = grid[i][j];
                count++;
            }
        }

        Arrays.sort(arr);

        Set<Integer> newSet = new HashSet<>();
        for (int n: arr) {
            newSet.add(n);
        }

        int count2 = 1;
        for (int j : newSet) {
            if (j != count2) {
                result[1] = count2;
                break;
            }
            count2++;
        }

        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] == arr[i + 1]) {
                result[0] = arr[i];
                break;
            }
        }

        return result;
    }
    public static int sumBase(int n, int k) {
        int sum = 0;
        while (n != 0){
            int reminder = n % k;
            sum += reminder;
            n /= k;
        }
        return sum;
    }
}
