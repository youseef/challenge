package day22;

import java.util.*;

public class Main {
    public static void main(String[] arg){
        System.out.println(clearDigits("cb43"));
    }
    public static int numOfPairs(String[] nums, String target) {
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums.length; j++) {
                if((i != j) && ((nums[i] + nums[j]).equals(target))){
                    count++;
                }
            }
        }
        return count;
    }
    public static int minAddToMakeValid(String s) {
        int ans = 0;
        int bal = 0;
        for(int i = 0; i < s.length(); i++){
            bal += s.charAt(i) == '(' ? 1 : -1;
            if(bal == -1){
                ans++;
                bal++;
            }
        }
        return ans + bal;
    }
    public static String sortVowels(String s) {
        List<Character> vowels = new ArrayList<>();
        for (char ch:s.toCharArray()) {
            if(isVowel(ch)){
                vowels.add(ch);
            }
        }
        Collections.sort(vowels);
        StringBuilder stringBuilder = new StringBuilder();
        int counter = 0;
        for (char ch: s.toCharArray()){
            if(isVowel(ch)){
                stringBuilder.append(vowels.get(counter));
                counter++;
            }else
                stringBuilder.append(ch);

        }
        return stringBuilder.toString();
    }

    public static boolean isVowel(Character value){
        if (value == 'a' || value == 'e' || value == 'i' ||
                value == 'o' || value == 'u' || value == 'A' ||
                value == 'E' || value == 'O' || value == 'I' || value == 'U'){
            return true;
        }
        return false;
    }

    public static String decodeMessage(String key, String message) {
        StringBuilder str = new StringBuilder();
        List<Character> alphabet = new ArrayList<>();
        Set<Character> seen = new HashSet<>();
        Map<Character, Character> decode = new HashMap<>();
        for (char ch = 'a'; ch <= 'z'; ch++) {
            alphabet.add(ch);
        }
        int alphaIndex = 0;
        for (char ch: key.toCharArray()) {
            if(ch != ' '&& !seen.contains(ch)){
                decode.put(ch, alphabet.get(alphaIndex));
                seen.add(ch);
                alphaIndex++;
                if(alphaIndex >= 26){
                    break;
                }
            }
        }
        for (char ch:message.toCharArray()) {
            if(ch == ' '){
                str.append(' ');
            }else
                str.append(decode.get(ch));
        }
        return str.toString();
    }
    public int maxDepth(String s) {
        int maxDepth = 0;
        int currentDepth = 0;
        for (char c : s.toCharArray()) {
            if (c == '(') {
                currentDepth++;
                maxDepth = Math.max(maxDepth, currentDepth);
            } else if (c == ')') {
                currentDepth--;
            }
        }
        return maxDepth;
    }
    public static String kthDistinct(String[] arr, int k) {
        List<String> arrays = new ArrayList<>();
        Map<String, Integer> frequencyMap = new HashMap<>();
        for (String s: arr) {
            frequencyMap.put(s, frequencyMap.getOrDefault(s,0)+1);
        }
        for (String s: arr) {
            if(frequencyMap.get(s) == 1){
                arrays.add(s);
            }
        }
        if(arrays.size() >= k){
            return arrays.get(k-1);
        }
        return " ";
    }
    public static String clearDigits(String s) {
        Stack<Character> stack = new Stack<>();
        for (char ch: s.toCharArray()) {
            if(Character.isDigit(ch)){
                if(!stack.isEmpty()){
                    stack.pop();
                }
            }
            else
                stack.push(ch);
        }
        StringBuilder str = new StringBuilder();
        for (char ch: stack) {
            str.append(ch);
        }
        return str.toString();
    }
}
