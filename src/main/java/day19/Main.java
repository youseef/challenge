package day19;

import java.util.Arrays;

public class Main {
    public static void main(String[] args){
        System.out.println(Arrays.toString(countBits(5)));
    }
    public static int maxRepeating(String sequence, String word) {
        int counts = 0;
        StringBuilder st = new StringBuilder();
        st.append(word);
        while (sequence.contains(st)){
            counts++;
            st.append(word);
        }
        return counts;
    }
    public static int[] countBits(int n) {
        int[] ans = new int[n+1];
        for (int i = 0; i <= n; i++) {
            String binaryString = Integer.toBinaryString(i);
            int count = 0;
            char[] chars = binaryString.toCharArray();
            for (char ch: chars) {
                if(ch == '1')
                    count++;
            }
            ans[i] = count;
        }
        return ans;
    }

}
