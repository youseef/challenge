package day11;

import java.net.BindException;
import java.net.Inet4Address;
import java.util.*;

public class Main {
    public static void main(String[] arg) {
        System.out.println(isHappy(2));
    }

    public static boolean isUgly(int n) {
        if (n <= 0) {
            return false;
        }
        while (n % 2 == 0) {
            n /= 2;
        }
        while (n % 3 == 0) {
            n /= 3;
        }
        while (n % 5 == 0) {
            n /= 5;
        }
        return n == 1;
    }

    public static int romanToInt(String s) {
        if (s == null || s.isEmpty()) {
            return 0;
        }

        Map<Character, Integer> romanValues = new HashMap<>();
        romanValues.put('I', 1);
        romanValues.put('V', 5);
        romanValues.put('X', 10);
        romanValues.put('L', 50);
        romanValues.put('C', 100);
        romanValues.put('D', 500);
        romanValues.put('M', 1000);

        int result = 0;

        for (int i = 0; i < s.length(); i++) {
            char currentChar = s.charAt(i);
            int currentValue = romanValues.get(currentChar);

            if (i < s.length() - 1) {
                char nextChar = s.charAt(i + 1);
                int nextValue = romanValues.get(nextChar);

                if (currentValue < nextValue) {
                    result += (nextValue - currentValue);
                    i++;
                } else {
                    result += currentValue;
                }
            } else {
                result += currentValue;
            }
        }

        return result;
    }

    public static List<String> splitWordsBySeparator(List<String> words, char separator) {
        List<String> seperatedList = new ArrayList<>();
        int beginning = 0;
        for (String word : words) {
            for (int i = 0; i < word.length(); i++) {
                if (word.charAt(i) == separator) {
                    String divided = word.substring(beginning, i);
                    if (!divided.isEmpty()) {
                        seperatedList.add(divided);
                    }
                    beginning = i + 1;
                }
                if (i == word.length() - 1) {
                    String divided = word.substring(beginning);
                    if (!divided.isEmpty()) {
                        seperatedList.add(divided);
                    }
                }
            }
            beginning = 0;
        }
        return seperatedList;
    }

    public static int countGoodSubstrings(String s) {
        int count = 0;

        for (int i = 0; i <= s.length() - 3; i++) {
            String subs = s.substring(i, i + 3);
            if (isValid(subs)) {
                count++;
            }
        }
        return count;
    }

    private static boolean isValid(String subs) {
        return subs.charAt(0) != subs.charAt(1) && subs.charAt(1) != subs.charAt(2) && subs.charAt(0) != subs.charAt(2);
    }

    public static int removeDuplicates(int[] nums) {
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            if (i > 0 && i < nums.length - 1 && nums[i] == nums[i - 1] && nums[i] == nums[i + 1]) {
                continue;
            }
            nums[count++] = nums[i];
        }
        return count;
    }

    public static void rotate(int[] nums, int k) {
        int n = nums.length;
        k = k % n; // handle cases where k is larger than the array size

        reverse(nums, 0, n - 1);          // Reverse the entire array
        reverse(nums, 0, k - 1);           // Reverse the first k elements
        reverse(nums, k, n - 1);           // Reverse the remaining elements
    }

    private static void reverse(int[] nums, int start, int end) {
        while (start < end) {
            int temp = nums[start];
            nums[start] = nums[end];
            nums[end] = temp;
            start++;
            end--;
        }
    }

    public static boolean canConstruct(String ransomNote, String magazine) {
        int[] charCount = new int[26];
        for (char ch : magazine.toCharArray()) {
            charCount[ch - 'a']++;
        }
        for (char ch : ransomNote.toCharArray()) {
            if (charCount[ch - 'a'] > 0) {
                charCount[ch - 'a']--;
            } else {
                return false;
            }
        }

        return true;
    }

    public static boolean isIsomorphic(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }

        Map<Character, Character> map1 = new HashMap<>();
        Map<Character, Character> map2 = new HashMap<>();

        for (int i = 0; i < s.length(); i++) {
            char charS = s.charAt(i);
            char charT = t.charAt(i);

            if (map1.containsKey(charS)) {
                if (map1.get(charS) != charT) {
                    return false;
                }
            } else {
                map1.put(charS, charT);
            }
            if (map2.containsKey(charT)) {
                if (map2.get(charT) != charS) {
                    return false;
                }
            } else {
                map2.put(charT, charS);
            }
        }

        return true;
    }

    public static boolean wordPattern(String pattern, String s) {
        List<String> words = List.of(s.split(" "));
        if(pattern.length() != words.size()){
            return false;
        }
        Map<Character, String> map = new HashMap<>();
        for (int i = 0; i < words.size(); i++) {
            char patternChar = pattern.charAt(i);
            String matchingWord = words.get(i);

            if(map.containsKey(patternChar)){
                if(!Objects.equals(map.get(patternChar), matchingWord)){
                    return false;
                }
            }else
                map.put(patternChar, matchingWord);
        }
        for (int i = 0; i < map.size(); i++) {
            for (int j = i+1; j < map.size(); j++) {
                if(Objects.equals(map.get(pattern.charAt(i)), map.get(pattern.charAt(j))) && pattern.charAt(i) != pattern.charAt(j)){
                    return false;
                }
            }
        }
        return true;
    }
    public static boolean isHappy(int n) {
        HashSet<Integer> seen = new HashSet<>();
        while (n != 1 && !seen.contains(n)){
            seen.add(n);
            n = getNext(n);
        }
        return n ==1;
    }


    public static int getNext(int n) {
        int sum = 0;
        while (n > 0){
            int digit = n %10;
            sum += digit * digit;
            n /= 10;
        }
        return sum;
    }
}
