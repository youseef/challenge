package day15;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] arg){
        System.out.println(busyStudent(new int[]{1,2,3}, new int[]{3,2,7}, 4));
    }
    public static int[][] flipAndInvertImage(int[][] image) {
        for (int[] arr: image) {
            int left = 0;
            int right = arr.length-1;
            while (left<right){
                int temp = arr[left];
                arr[left] = arr[right];
                arr[right] = temp;
                left++;
                right--;
            }
            for (int i = 0; i < arr.length; i++) {
                if(arr[i] == 1){
                    arr[i] = 0;
                }else
                    arr[i] = 1;
            }
        }
        return image;
    }
    public static int maximizeSum(int[] nums, int k) {
        Arrays.sort(nums);
        int sum = 0;
        for (int i = 0; i < k; i++) {
            sum += nums[nums.length-1];
            nums[nums.length-1] = nums[nums.length -1]+1;
        }
        return sum;
    }
    public static int[] findIntersectionValues(int[] nums1, int[] nums2) {
        int[] result = new int[2];
        int sum = 0;
        int sum2 = 0;
        for (int num: nums1) {
            for (int i = 0; i < nums2.length; i++) {
                if(num == nums2[i]){
                    sum++;
                    break;
                }
            }
        }
        for (int num: nums2) {
            for (int i = 0; i < nums1.length; i++) {
                if(num == nums1[i]){
                    sum2++;
                    break;
                }
            }
        }
        result[0] = sum;
        result[1] = sum2;
        return result;
    }
    public static int countPairs(int[] nums, int k) {
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            for (int j = i+1; j < nums.length; j++) {
                if(nums[i] == nums[j] && j > i && (j * i)%k == 0){
                    count++;
                }
            }
        }
        return count;
    }
    public static List<List<Integer>> findDifference(int[] nums1, int[] nums2) {
        List<List<Integer>> answers = new ArrayList<>();
        List<Integer> unique = adding(nums1, nums2);
        answers.add(unique);
        List<Integer> unique1 = adding(nums2, nums1);
        answers.add(unique1);
        return answers;
    }

    public static List<Integer> adding(int[] nums1, int[] nums2){
        List<Integer> uniques = new ArrayList<>();
        boolean found;
        for (int num:nums1) {
            found = false;
            for (int i = 0; i < nums2.length; i++) {
                if (num == nums2[i]) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                uniques.add(num);
            }
        }
        return uniques;
    }

    public static int heightChecker(int[] heights) {
        int count = 0;
        int[] arr = new int[heights.length];
        for (int i = 0; i < heights.length; i++) {
            arr[i] = heights[i];
        }
        Arrays.sort(heights);
        for (int i = 0; i < heights.length; i++) {
            if(arr[i] != heights[i]){
                count++;
            }
        }
        return count;
    }
    public static List<Integer> findPeaks(int[] mountain) {
        List<Integer> numbers = new ArrayList<>();
        for (int i = 1; i < mountain.length-1; i++) {
            if(mountain[i-1] < mountain[i] && mountain[i] > mountain[i+1]){
                numbers.add(i);
            }
        }
        return numbers;
    }
    public static int busyStudent(int[] startTime, int[] endTime, int queryTime) {
        int count = 0;
        for (int i = 0; i < startTime.length; i++) {
            if(startTime[i] <= queryTime && endTime[i] >= queryTime){
                count++;
            }
        }
        return count;
    }
}
