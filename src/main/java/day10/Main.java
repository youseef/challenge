package day10;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args){
        System.out.println(repeatedCharacter("abccbaacz"));
    }
    public static
    boolean isSumEqual(String firstWord, String secondWord, String targetWord) {
        StringBuilder stringBuilder = charToIntNumber(firstWord);
        StringBuilder stringBuilder1 = charToIntNumber(secondWord);
        StringBuilder lastOne = charToIntNumber(targetWord);

        int n1 = Integer.parseInt(stringBuilder.toString());
        int n2 = Integer.parseInt(stringBuilder1.toString());
        int n3 = Integer.parseInt(lastOne.toString());


        return n1 + n2 == n3;
    }
    public static StringBuilder charToIntNumber(String word){
        StringBuilder stringBuilder = new StringBuilder();
        for (char ch: word.toCharArray()) {
            int n = ch - 'a';
            stringBuilder.append(n);
        }
        return stringBuilder;
    }
    public static int vowelStrings(String[] words, int left, int right) {
        int count = 0;
        Set<Character> set = new HashSet<>();
        set.add('a');
        set.add('e');
        set.add('i');
        set.add('u');
        set.add('o');
        for (int i = left; i <= right; i++) {
            char ch1 = words[i].charAt(0);
            char ch2 = words[i].charAt(words[i].length()-1);
            if(set.contains(ch1) && set.contains(ch2)){
                count++;
            }
        }
        return count;
    }
    public static int percentageLetter(String s, char letter) {
        int count = 0;
        for (char ch: s.toCharArray()) {
            if(ch == letter){
                count++;
            }
        }
        return Math.round((float) count /s.length() * 100);
    }
    public static char repeatedCharacter(String s) {
        int[] lastIndex = new int[26];

        for (int i = 0; i < s.length(); i++) {
            char currentChar = s.charAt(i);
            if (lastIndex[currentChar - 'a'] != 0) {
                return currentChar;
            }
            lastIndex[currentChar - 'a'] = i + 1;
        }
        return '\0';
    }
}
