package day18;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class Main {
    public static void main(String[] args){
        System.out.println(uniqueMorseRepresentations(new String[]{"a"}));
    }
    public static int[][] largestLocal(int[][] grid) {
        int n = grid.length;
        int[][] result = new int[n-2][n-2];
        for (int i = 0; i < n - 2; i++) {
            for (int j = 0; j < n -2; j++) {
                int maxvalue = Integer.MIN_VALUE;
                for (int k = 0; k < i + 3; k++) {
                    for (int l = 0; l < j + 3; l++) {
                        maxvalue = Math.max(maxvalue, grid[k][l]);
                    }
                }
                result[i][j] = maxvalue;
            }
        }
        return result;
    }
    public static int maxWidthOfVerticalArea(int[][] points) {

        Arrays.sort(points ,(a, b) ->Integer.compare(a[0], b[0]));

        int maxNumber = 0;
        for (int i = 1; i <points.length; i++) {
            maxNumber = Math.max( maxNumber,points[i][0] - points[i -1][0]);
        }
        return maxNumber;
    }

    public static int numberOfPairs(int[] nums1, int[] nums2, int k) {
        int counts = 0;
        for (int value : nums1) {
            for (int i : nums2) {
                if (value % (i * k) == 0) {
                    counts++;
                }
            }
        }
        return counts;
    }
    public static int minOperations(int[] nums, int k) {
        int counts = 0;
        for (int n: nums) {
            if(n < k)
                counts++;
        }
        return counts;
    }
    public static int duplicateNumbersXOR(int[] nums) {
        int xor = 0;
        for (int i = 0; i < nums.length; i++) {
            for (int j = i+1; j < nums.length; j++) {
                if(nums[i] == nums[j]){
                    xor = xor ^ nums[i];
                }
            }
        }
        return xor;
    }
    public static int uniqueMorseRepresentations(String[] words) {
        HashSet<String> transformations = new HashSet<>();
        HashMap<Character, String> morseCode = new HashMap<>();

        morseCode.put('a', ".-");
        morseCode.put('b', "-...");
        morseCode.put('c', "-.-.");
        morseCode.put('d', "-..");
        morseCode.put('e', ".");
        morseCode.put('f', "..-.");
        morseCode.put('g', "--.");
        morseCode.put('h', "....");
        morseCode.put('i', "..");
        morseCode.put('j', ".---");
        morseCode.put('k', "-.-");
        morseCode.put('l', ".-..");
        morseCode.put('m', "--");
        morseCode.put('n', "-.");
        morseCode.put('o', "---");
        morseCode.put('p', ".--.");
        morseCode.put('q', "--.-");
        morseCode.put('r', ".-.");
        morseCode.put('s', "...");
        morseCode.put('t', "-");
        morseCode.put('u', "..-");
        morseCode.put('v', "...-");
        morseCode.put('w', ".--");
        morseCode.put('x', "-..-");
        morseCode.put('y', "-.--");
        morseCode.put('z', "--..");

        for (String word: words) {
            StringBuilder stringBuilder = new StringBuilder();
            char[] arr = word.toCharArray();
            for (char ch: arr) {
                if(morseCode.containsKey(ch)){
                    stringBuilder.append(morseCode.get(ch));
                }
            }
            transformations.add(stringBuilder.toString());
        }

        return transformations.size();
    }
}
